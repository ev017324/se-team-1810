#include <iostream> 

#include <string> 

#include <vector> 

 
 

using namespace std; 

 
 

class PatientRecord { 

public: 

int pid; 

string name; 

string dob; 

string address; 

string email; 

string phone; 

string blood_group; 

 
 

PatientRecord(int id, string n, string b, string a, string e, string p, string bg) { 

pid = id; 

name = n; 

dob = b; 

address = a; 

email = e; 

phone = p; 

blood_group = bg; 

} 

}; 

 
 

class PatientRecordManager { 

public: 

vector<PatientRecord> patientRecords; 

 
 

void addPatientRecord() { 

// Create a new patient record 

int id; 

string n, b, a, e, p, bg; 

 
 

cout << "Enter Patient ID: "; 

cin >> id; 

cout << "Enter Patient Name: "; 

cin.ignore(); 

getline(cin, n); 

cout << "Enter Patient DOB: "; 

getline(cin, b); 

cout << "Enter Patient Address: "; 

getline(cin, a); 

cout << "Enter Patient Email: "; 

getline(cin, e); 

cout << "Enter Patient Phone: "; 

getline(cin, p); 

cout << "Enter Patient Blood Group: "; 

getline(cin, bg); 

 
 

PatientRecord newRecord(id, n, b, a, e, p, bg); 

patientRecords.push_back(newRecord); 

} 

 
 

void displayPatientRecord() { 

// Get the patient ID to display 

int pid; 

cout << "Enter Patient ID: "; 

cin >> pid; 

 
 

// Search for the patient record with the given ID 

bool found = false; 

for (PatientRecord record : patientRecords) { 

if (record.pid == pid) { 

found = true; 

cout << "Patient ID: " << record.pid << endl; 

cout << "Patient Name: " << record.name << endl; 

cout << "Patient DOB: " << record.dob << endl; 

cout << "Patient Address: " << record.address << endl; 

cout << "Patient Email: " << record.email << endl; 

cout << "Patient Phone: " << record.phone << endl; 

cout << "Patient Blood Group: " << record.blood_group << endl; 

break; 

} 

} 

 
 

// If the patient record was not found, display an error message 

if (!found) { 

cout << "Patient record not found." << endl; 

} 

} 

 
 

bool checkAuthorization() { 

// Check user's authorization level 

int authorizationLevel; 

cout << "Enter your authorization level (1 for read-only access, 2 for read/write access): "; 

cin >> authorizationLevel; 

 
 

// Perform a simple authorization check using an if-else condition 

if (authorizationLevel == 1) { 

return false; 

} else if (authorizationLevel == 2) { 

string password; 

cout << "Enter the password: "; 

cin >> password; 

return (password == "cs1se20"); 

} else { 

cout << "Invalid authorization level." << endl; 

return false; 

} 

} 

 
 

void editPatientRecord() { 

// Edit patient ID  

int pid; 

cout << "Enter Patient ID to edit: "; 

cin >> pid; 

 
 

// Search for the patient record with the given ID 

bool found = false; 

for (PatientRecord& record : patientRecords) { 

if (record.pid== pid) { 

found = true; 

// Check authorization before allowing edits 

if (checkAuthorization()) { 

// Get the new patient information 

string n, b, a, e, p, bg; 

cout << "Enter New Patient Name: "; 

cin.ignore(); 

getline(cin, n); 

cout << "Enter New Patient DOB: "; 

getline(cin, b); 

cout << "Enter New Patient Address: "; 

getline(cin, a); 

cout << "Enter New Patient Email: "; 

getline(cin, e); 

cout << "Enter New Patient Phone: "; 

getline(cin, p); 

cout << "Enter New Patient Blood Group: "; 

getline(cin, bg); 

 
 

// Update the patient record 

record.name = n; 

record.dob = b; 

record.address = a; 

record.email = e; 

record.phone = p; 

record.blood_group = bg; 

 
 

cout << "Patient record updated successfully." << endl; 

} else { 

cout << "You are not authorized to edit this patient record." << endl; 

} 

 
 

break; 

} 

} 

 
 

// If the patient record is not found, an error message is shown 

if (!found) { 

cout << "Patient record not found." << endl; 

} 

} 

 
 

void removePatientRecord() { 

// Get the patient ID to remove 

int pid; 

cout << "Enter Patient ID to remove: "; 

cin >> pid; 

 
 

// Search for the patient record with the given ID 

for (auto it = patientRecords.begin(); it != patientRecords.end(); ++it) { 

if (it->pid == pid) { 

// Check authorization before allowing removal 

if (checkAuthorization()) { 

// Erase the patient record from the vector 

patientRecords.erase(it); 

cout << "Patient record removed successfully." << endl; 

} else { 

cout << "You are not authorized to remove this patient record." << endl; 

} 

 
 

return; 

} 

} 

 
 

// If the patient record was not found, display an error message 

cout << "Patient record not found." << endl; 

} 

}; 

 
 

int main() { 

PatientRecordManager manager; 

// Patient records 

manager.patientRecords.push_back(PatientRecord(1, "Marc Spector", "01/01/2002", "426 Street Lane", "email@email.com", "1234567890", "A+")); 

manager.patientRecords.push_back(PatientRecord(2, "Steven Grant", "05/07/1982", "51a Knighthouse", "gmail@email.com", "2345678910", "B-")); 

manager.patientRecords.push_back(PatientRecord(3, "Billy Jean", "24/12/1987", "98 Lane Avenue", "email@mail.com", "098765322", "O+")); 

 
 

// The program's main menu  

int choice = 0; 

while (choice != 5) { 

cout << endl; 

cout << "Patient management system" << endl; 

cout << "------------------------------------------" << endl; 

cout << "1. Add a new patient record" << endl; 

cout << "2. Display patient information" << endl; 

cout << "3. Edit a patient record" << endl; 

cout << "4. Remove a patient record" << endl; 

cout << "5. Exit the program" << endl; 

cout << "------------------------------------------" << endl; 

 
 

cin >> choice; 

 
 

switch (choice) { 

case 1: 

manager.addPatientRecord(); 

break; 

case 2: 

manager.displayPatientRecord(); 

break; 

case 3: 

manager.editPatientRecord(); 

break; 

case 4: 

manager.removePatientRecord(); 

break; 

case 5: 

// Exit the program 

cout << "Goodbye!" << endl; 

return 0; 

default: 

cout << "Invalid choice." << endl; 

} 

} 

} 

 
